'use strict';

const http = require('http');
const fs = require('fs');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const Busboy = require('busboy');
const cloud = require('../cloud/s3');

dotenv.config();

const s3config = {
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_ACCESS_KEY,
  region: process.env.REGION,
};

const port = process.env.PORT || 5000;

const sendFile = (filename, type, res) => {
  const stat = fs.statSync(filename);
  res.writeHead(200, { 'Content-Type': type, 'Content-Length': stat.size });
  const readStream = fs.createReadStream(filename);
  readStream.pipe(res);
};
const uploadFile = async (req, res) => {
  const busboy = new Busboy({ headers: req.headers });
  busboy.on('file', async (fieldname, file, filename, encoding, mimetype) => {
    console.log('=== on file ===');
    const s3 = cloud.open(s3config);
    // upload file
    s3.upload(process.env.BUCKET,
      file,
      'folder/' + filename, mimetype,
      err => { if (err) console.log(err); });
  });
  busboy.on('field', (name, value) => {
    // print the rest of fields from form
    console.log('=== on field ====');
    console.log(name, value);
  });
  busboy.on('finish', () => {
    console.log('=== Uploading finished ===');
    // send message to client
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('DONE');
  });
  return req.pipe(busboy);
};
const badRequest404 = res => {
  res.writeHead(404, { 'Content-Type': 'text/plain' });
  res.end('404: bad request');
};

const getRouting = {
  '/': sendFile.bind(null,
    process.env.ROOT_DIR + 'views/home.html',
    'text/html'),
  '/favicon.ico': sendFile.bind(null,
    process.env.ROOT_DIR + 'views/img/favicon.png',
    'image/png'),
};
const postRouting = {
  '/uploadfile': uploadFile,
};

const server = http.createServer((req, res) => {
  console.log(req.method);
  console.log(req.url);
  if (req.method === 'GET') {
    const action = getRouting[req.url];
    if (!action) {
      badRequest404(res);
      return;
    }
    action(res);
  } else if (req.method === 'POST') {
    const action = postRouting[req.url];
    if (!action) {
      badRequest404(res);
      return;
    }
    action(req, res);
  } else {
    badRequest404(res);
  }
});

server.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
