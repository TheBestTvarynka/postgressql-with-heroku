const fs = require('fs');
const dotenv = require('dotenv');
const AWS = require('aws-sdk');

dotenv.config();

const s3 = new AWS.S3({
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_ACCESS_KEY,
  region: process.env.REGION,
});

const downloadFile = (filepath, filename) => {
  const params = {
    Bucket: process.env.BUCKET,
    Key: filename
  };
  // download file
  s3.getObject(params, (err, data) => {
    if (err) {
      console.log(err);
      return;
    }
    // write file
    fs.writeFile(filepath, data.Body, err => {
      if (err) 
        console.log(err);
    });
    console.log('writing');
  });
};

downloadFile(process.env.ROOT_DIR + 'files/download.png', 'VIM.png');
