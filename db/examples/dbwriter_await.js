'use strict';

const dbwriter = require('../dbwriter');
const dotenv = require('dotenv');

dotenv.config();

const dbconfig = {
  connectionString: process.env.DATABASE_URL,
  ssl: true
};

(async () => {
  const pg = dbwriter.open(dbconfig);
  const cursor = pg.insert('example');
  console.log('==== starting ===');
  let res = await cursor.value({ id: '8', name: 'qwe', count: '86' },
      { id: 'value', name: 'value', count: 'value' });
  console.log('Result of inserting:', res);
  console.log('==== ending ===');

  const update = pg.update('example');
  console.log('==== starting ===');
  res = await update.where({ name: 'kk6g' })
    .set({ id: '4', name: 'kk6g', count: '102' },
      { id: 'value', name: 'value', count: 'value' });
  pg.close();
  console.log('Result of updating:', res);
  console.log('==== ending ===');
})();
