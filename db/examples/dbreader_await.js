'use strict';

const dbreader = require('../dbreader');
const dotenv = require('dotenv');

dotenv.config();

const dbconfig = {
  connectionString: process.env.DATABASE_URL,
  ssl: true
};

(async () => {
  const pg = dbreader.open(dbconfig);
  const cursor = pg.select('example')
    .where({ count: '<98' })
    .order('id', false)
    .fields([ 'name', 'count' ])
    .limit(5);
  console.log('==== starting ===');
  const res = await cursor;
  pg.close();
  console.log('res', res);
  console.log('==== ending ===');
})();
