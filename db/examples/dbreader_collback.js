'use strict';

const dbreader = require('../dbreader');
const dotenv = require('dotenv');

dotenv.config();

const dbconfig = {
  connectionString: process.env.DATABASE_URL,
  ssl: true
};

const table = 'example';
// #1
const pg = dbreader.open(dbconfig);
pg.select(table)
  .where({ name: 'lmemve' })
//.where statement. { key: 'value', ... }. value can be:
    // 'just_string'
    // '>=value'
    // '<=value'
    // '<>value'
    // '@>value'
    // '<value'
    // '>value'
    // 'INvalue'
//.limit(5) query only 5 rows
//.order(name, direction) set order for query:
                       // name - name of column, 
                       // direction - can be ASC or DESC
  .then(res => {
    pg.close(); // alwaus close the connection to db
    console.log(res);
  });
