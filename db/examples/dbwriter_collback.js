'use strict';

const dbwriter = require('../dbwriter');
const dotenv = require('dotenv');

dotenv.config();

const dbconfig = {
  connectionString: process.env.DATABASE_URL,
  ssl: true
};

const table = 'example';
const pg = dbwriter.open(dbconfig);
// insert exapmle
const inserter = pg.insert(table);
inserter.value({ id: 'nextval("new_id")', name: 'veniud', likedbooks: ['2', '4'] },
               { id: 'function', name: 'value', likedbooks: 'array' })
        .then(res => {
          console.log(res);
        });
// if we want to add a few values, just call .value mathod a few times

// update example
const updater = pg.update(table);
updater.where({ id: '43' })
       // where statement such as dbreader wheren statement. see db/examples/dbreader_example.js
       .set({ name: 'poxw' }, { name: 'value' })
       // set have parameters such as value parameters in inserter
       .then(res => {
         pg.close();
         console.log(res);
       });
