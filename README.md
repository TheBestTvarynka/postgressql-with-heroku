# PostgreSQL, S3 with node js

Content:
* download file from amazon s3 bucket: `download/downloadfile.js`
* upload file to amazon s3 bucket:
  * from form: `upload/uploadfilefromform.js` and `views/home.html`
  * static file from filesystem: `upload/uploadstaticfile.js`
* read data from database: `db/examples/dbreader_callback.js` and `db/examples/dbreader_await.js`
* insert and update data in database: `db/examples/dbwriter_callback.js` and `db/examples/dbwriter_await.js`

## Connect PostgreSQL to Heroku app

1. From site:
* go to your application on herocu site. you can find it [there](https://dashboard.heroku.com/)
* then go to addons page (Configure add-ons): ![](views/img/1.png)
* find 'postgres' and select `Heroku Postgres`: ![](views/img/2.png)
* go to addon page: ![](views/img/3.png)
All done!

2. From terminal, using Heroku CLI:
* Use the heroku addons command to determine whether your app already has Heroku Postgres provisioned: `heroku addons`
* heroku addons:create heroku-postgresql: 'PLAN_NAME'. You can see all plans on [this](https://elements.heroku.com/addons/heroku-postgresql) page.
For example, to provision a hobby-dev plan database: `heroku addons:create heroku-postgresql:hobby-dev`.
All done! For more commands see link below.

Informtion about database host, port, user, password etc you can find in `Settings -> View Credentials`.
Example: ![](views/img/4.png)
With this parameters you can connect to PostgreSQL databse:
* from terminal: `psql -d ${Database} -U ${User} ${Password}`
* with node js (`pg` module must be installed. If not, type `npm install pg`):
```
'use strict';
const { Pool } = require('pg');
const pool = new Pool({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_DTABASE,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
});
const sql = 'SELECT * FROM pg_tables WHERE id = $1';
pool.query(sql, [ '32' ], (err, res) => {
  if (err) {
    throw err;
  }
  console.dir({ res });
  console.table(res.fields);
  console.table(res.rows);
  pool.end();
});
```
Official documentation about PostgreSQL on Heroku site: (<https://devcenter.heroku.com/articles/heroku-postgresql>)

## How to use Amason S3 Buckets


## Meta

Pavlo Myroniyk – @TheBestTvarynka, [pspos.developqkation@gmail.com](mailto:pspos.developqkation@gmail.com)

**dbreader based on [this](https://github.com/HowProgrammingWorks/Databases/blob/master/JavaScript/db.js) code.**
dbwriter is written analogous to dbreader

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
